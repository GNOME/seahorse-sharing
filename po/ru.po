# Russian translation for seahorse-sharing.
# Copyright (C) 2012 seahorse-sharing's COPYRIGHT HOLDER
# This file is distributed under the same license as the seahorse-sharing package.
# Yuri Myasoedov <omerta13@yandex.ru>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: seahorse-sharing master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-11-12 07:57+0000\n"
"PO-Revision-Date: 2012-12-22 12:18+0300\n"
"Last-Translator: Yuri Myasoedov <omerta13@yandex.ru>\n"
"Language-Team: Russian <gnome-cyr@gnome.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../daemon/seahorse-daemon.c:55
msgid "Do not run seahorse-daemon as a daemon"
msgstr "Не запускать seahorse-daemon в качестве службы"

#: ../daemon/seahorse-daemon.c:84
msgid "couldn't fork process"
msgstr "не удалось создать новый процесс"

#: ../daemon/seahorse-daemon.c:90
msgid "couldn't create new process group"
msgstr "не удалось создать новую группу процессов"

#: ../daemon/seahorse-daemon.c:319
msgid "Key Sharing Daemon (Seahorse)"
msgstr "Служба доступа к ключам (Seahorse)"

#: ../daemon/seahorse-sharing.c:167
#: ../daemon/seahorse-sharing.c:383
msgid "Couldn't share keys"
msgstr "Не удалось открыть доступ к ключам"

#: ../daemon/seahorse-sharing.c:168
msgid "Can't publish discovery information on the network."
msgstr "Не удалось опубликовать информацию в сети."

#.
#. * Translators: The %s will get filled in with the user name
#. * of the user, to form a genitive. If this is difficult to
#. * translate correctly so that it will work correctly in your
#. * language, you may use something equivalent to
#. * "Shared keys of %s".
#.
#: ../daemon/seahorse-sharing.c:196
#, c-format
msgid "%s's encryption keys"
msgstr "Ключи шифрования пользователя %s"

#: ../daemon/seahorse-sharing.desktop.in.h:1
msgid "Seahorse Sharing Daemon"
msgstr "Служба доступа к ключам Seahorse"

#: ../libegg/eggdesktopfile.c:165
#, c-format
msgid "File is not a valid .desktop file"
msgstr "Файл не является корректным файлом .desktop"

#: ../libegg/eggdesktopfile.c:188
#, c-format
msgid "Unrecognized desktop file Version '%s'"
msgstr "Неизвестная версия файла .desktop «%s»"

#: ../libegg/eggdesktopfile.c:968
#, c-format
msgid "Starting %s"
msgstr "Запуск %s"

#: ../libegg/eggdesktopfile.c:1110
#, c-format
msgid "Application does not accept documents on command line"
msgstr "Приложение не поддерживает открытие документов через командную строку"

#: ../libegg/eggdesktopfile.c:1178
#, c-format
msgid "Unrecognized launch option: %d"
msgstr "Неизвестный параметр запуска: %d"

#: ../libegg/eggdesktopfile.c:1383
#, c-format
msgid "Can't pass document URIs to a 'Type=Link' desktop entry"
msgstr "Невозможно передать URI в запись «Type=Link» файла .desktop"

#: ../libegg/eggdesktopfile.c:1404
#, c-format
msgid "Not a launchable item"
msgstr "Не может быть запущено"

#: ../libegg/eggsmclient.c:225
msgid "Disable connection to session manager"
msgstr "Отключить от диспетчера сеансов"

#: ../libegg/eggsmclient.c:228
msgid "Specify file containing saved configuration"
msgstr "Указать файл с настройками"

#: ../libegg/eggsmclient.c:228
msgid "FILE"
msgstr "ФАЙЛ"

#: ../libegg/eggsmclient.c:231
msgid "Specify session management ID"
msgstr "Укажите ID диспетчера сеанса"

#: ../libegg/eggsmclient.c:231
msgid "ID"
msgstr "ID"

#: ../libegg/eggsmclient.c:252
msgid "Session management options:"
msgstr "Параметры управления сеансом:"

#: ../libegg/eggsmclient.c:253
msgid "Show session management options"
msgstr "Показать параметры управления сеансом"

