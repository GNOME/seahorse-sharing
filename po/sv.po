# Swedish translation for seahorse-sharing.
# Copyright © 2015 seahorse-sharing's COPYRIGHT HOLDER
# This file is distributed under the same license as the seahorse-sharing package.
# Josef Andersson <josef.andersson@fripost.org>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: seahorse-sharing master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-06-28 23:34+0000\n"
"PO-Revision-Date: 2015-06-28 20:19+0100\n"
"Last-Translator: Josef Andersson <josef.andersson@fripost.org>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../daemon/seahorse-daemon.c:52
msgid "Do not run seahorse-daemon as a daemon"
msgstr "Kör inte seahorse-deamon som en demon"

#: ../daemon/seahorse-daemon.c:81
msgid "couldn't fork process"
msgstr "kunde inte förgrena processen"

#: ../daemon/seahorse-daemon.c:87
msgid "couldn't create new process group"
msgstr "kunde inte skapa ny processgrupp"

#: ../daemon/seahorse-daemon.c:316
msgid "Key Sharing Daemon (Seahorse)"
msgstr "Nyckeldelningsdemon (Seahorse)"

#: ../daemon/seahorse-sharing.c:164 ../daemon/seahorse-sharing.c:380
msgid "Couldn't share keys"
msgstr "Kunde inte dela nycklar"

#: ../daemon/seahorse-sharing.c:165
msgid "Can't publish discovery information on the network."
msgstr "Kan inte publicera tjänsteidentifiering på nätverket."

#.
#. * Translators: The %s will get filled in with the user name
#. * of the user, to form a genitive. If this is difficult to
#. * translate correctly so that it will work correctly in your
#. * language, you may use something equivalent to
#. * "Shared keys of %s".
#.
#: ../daemon/seahorse-sharing.c:193
#, c-format
msgid "%s's encryption keys"
msgstr "Kryptonycklar för %s"

#: ../daemon/seahorse-sharing.desktop.in.h:1
msgid "Seahorse Sharing Daemon"
msgstr "Seahorse delningsdemon"

#: ../libegg/eggdesktopfile.c:164
#, c-format
msgid "File is not a valid .desktop file"
msgstr "Filen är inte en giltig .desktop-fil"

#: ../libegg/eggdesktopfile.c:187
#, c-format
msgid "Unrecognized desktop file Version '%s'"
msgstr "Okänd desktop-filversion ”%s”"

#: ../libegg/eggdesktopfile.c:967
#, c-format
msgid "Starting %s"
msgstr "Startar %s"

#: ../libegg/eggdesktopfile.c:1109
#, c-format
msgid "Application does not accept documents on command line"
msgstr "Programmet accepterar inte dokument på kommandoraden"

#: ../libegg/eggdesktopfile.c:1177
#, c-format
msgid "Unrecognized launch option: %d"
msgstr "Okänt startalternativ: %d"

#: ../libegg/eggdesktopfile.c:1382
#, c-format
msgid "Can't pass document URIs to a 'Type=Link' desktop entry"
msgstr "Kan inte skicka dokument-URI:er till ett ”Type=Link”-desktop-fält"

#: ../libegg/eggdesktopfile.c:1403
#, c-format
msgid "Not a launchable item"
msgstr "Inte ett startbart objekt"

#: ../libegg/eggsmclient.c:223
msgid "Disable connection to session manager"
msgstr "Inaktivera anslutning till sessionshanterare"

#: ../libegg/eggsmclient.c:226
msgid "Specify file containing saved configuration"
msgstr "Ange fil som innehåller sparad konfiguration"

#: ../libegg/eggsmclient.c:226
msgid "FILE"
msgstr "FIL"

#: ../libegg/eggsmclient.c:229
msgid "Specify session management ID"
msgstr "Ange sessionshanterings-ID"

#: ../libegg/eggsmclient.c:229
msgid "ID"
msgstr "ID"

#: ../libegg/eggsmclient.c:250
msgid "Session management options:"
msgstr "Sessionshanteringsalternativ:"

#: ../libegg/eggsmclient.c:251
msgid "Show session management options"
msgstr "Visa sessionshanteringsalternativ"
