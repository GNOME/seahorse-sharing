# Indonesian translation for seahorse-sharing.
# Copyright (C) 2013 seahorse-sharing's COPYRIGHT HOLDER
# This file is distributed under the same license as the seahorse-sharing package.
# Andika Triwidada <andika@gmail.com>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: seahorse-sharing gnome-3-4\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-08-18 13:42+0000\n"
"PO-Revision-Date: 2013-09-28 15:41+0700\n"
"Last-Translator: Andika Triwidada <andika@gmail.com>\n"
"Language-Team: Indonesian <gnome@i15n.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"

#: ../daemon/seahorse-daemon.c:55
msgid "Do not run seahorse-daemon as a daemon"
msgstr "Jangan jalankan seahorse-daemon sebagai daemon"

#: ../daemon/seahorse-daemon.c:84
msgid "couldn't fork process"
msgstr "tak bisa fork proses"

#: ../daemon/seahorse-daemon.c:90
msgid "couldn't create new process group"
msgstr "tak bisa membuat grup proses baru"

#: ../daemon/seahorse-daemon.c:319
msgid "Key Sharing Daemon (Seahorse)"
msgstr "Daemon Berbagai Pakai Kunci (Seahorse)"

#: ../daemon/seahorse-sharing.c:167 ../daemon/seahorse-sharing.c:383
msgid "Couldn't share keys"
msgstr "Tak bisa berbagi pakai kunci"

#: ../daemon/seahorse-sharing.c:168
msgid "Can't publish discovery information on the network."
msgstr "Tak bisa publikasikan informasi penemuan pada jaringan."

#.
#. * Translators: The %s will get filled in with the user name
#. * of the user, to form a genitive. If this is difficult to
#. * translate correctly so that it will work correctly in your
#. * language, you may use something equivalent to
#. * "Shared keys of %s".
#.
#: ../daemon/seahorse-sharing.c:196
#, c-format
msgid "%s's encryption keys"
msgstr "Kunci enkripsi %s"

#: ../daemon/seahorse-sharing.desktop.in.h:1
msgid "Seahorse Sharing Daemon"
msgstr "Seahorse Daemon Berbagi Pakai"

#: ../libegg/eggdesktopfile.c:165
#, c-format
msgid "File is not a valid .desktop file"
msgstr "Berkas bukan berkas .desktop yang valid"

#: ../libegg/eggdesktopfile.c:188
#, c-format
msgid "Unrecognized desktop file Version '%s'"
msgstr "Berkas desktop tak dikenal Versi '%s'"

#: ../libegg/eggdesktopfile.c:968
#, c-format
msgid "Starting %s"
msgstr "Memulai %s"

#: ../libegg/eggdesktopfile.c:1110
#, c-format
msgid "Application does not accept documents on command line"
msgstr "Aplikasi tak menerima dokumen pada baris perintah"

#: ../libegg/eggdesktopfile.c:1178
#, c-format
msgid "Unrecognized launch option: %d"
msgstr "Opsi peluncuran tak dikenal: %d"

#: ../libegg/eggdesktopfile.c:1383
#, c-format
msgid "Can't pass document URIs to a 'Type=Link' desktop entry"
msgstr "Tak bisa melewatkan URI dokumen ke entri dekstop 'Type=Link'"

#: ../libegg/eggdesktopfile.c:1404
#, c-format
msgid "Not a launchable item"
msgstr "Bukan butir yang dapat diluncurkan"

#: ../libegg/eggsmclient.c:225
msgid "Disable connection to session manager"
msgstr "Matikan koneksi ke manajer sesi"

#: ../libegg/eggsmclient.c:228
msgid "Specify file containing saved configuration"
msgstr "Nyatakan berkas yang memuat konfigurasi tersimpan"

#: ../libegg/eggsmclient.c:228
msgid "FILE"
msgstr "FILE"

#: ../libegg/eggsmclient.c:231
msgid "Specify session management ID"
msgstr "Nyatakan ID manajemen sesi"

#: ../libegg/eggsmclient.c:231
msgid "ID"
msgstr "ID"

#: ../libegg/eggsmclient.c:252
msgid "Session management options:"
msgstr "Opsi manajemen sesi:"

#: ../libegg/eggsmclient.c:253
msgid "Show session management options"
msgstr "Tampilkan opsi manajemen sesi"
